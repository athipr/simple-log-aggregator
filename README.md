# simple-log-aggregator

This app aims to simplify logging message aggregation and displaying on grafana dashboard. It collects logging message over http (ie. from fluent bit) and store in mongodb. It also acts as a data source for Grafana for displaying on portal.

To run/install from docker

docker run --rm -v `pwd`/config.json:/mnt/config/config.json -p 3001:3001 -it athipr/simple-log-aggregator:latest
