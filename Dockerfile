FROM ubuntu:latest
MAINTAINER Athip athipr@gmail.com
RUN apt update
RUN apt install curl -y
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get install -y nodejs
RUN mkdir /express
COPY package.json /express/
COPY app.js /express/
RUN cd /express && npm update

ENTRYPOINT node /express/app.js
