const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const parse = require('co-body');
const cron = require('node-cron');
const async = require('async');
const moment = require('moment');
const config = require('/mnt/config/config.json');
const mongojs = require('mongojs');
const db = mongojs(config.connection, ['logging']);

const http = require('http');
http.globalAgent.keepAlive = true;

const app = express();
const port = config.port;

app.use(cors());
app.use(morgan('combined'));

var jsonParser = bodyParser.json({limit: "50mb"});

app.options('/', function(req, res, next) { res.sendStatus(200); });
app.get('/', (req, res) => res.send('Up and running...'))

/*
var timeout = require('connect-timeout')
app.use('/', (req, res, next) => {
   req.setTimeout((2 * 60 * 1000) + 1);
   next();
}, timeout('2m'));
*/

app.post('/', async function (req, res) {
  var body = await parse.json(req, { limit: '50mb' });
  let date = new Date();
  let data = body;

  let ndata = [];
  for (let i = 0; i < data.length; i += 1)
  {
    let entry = data[i];
    let nentry= {};
    for (let key in entry) {
      nkey = key.replace('.', '/');
      nentry[nkey] = entry[key]
    }
    ndata.push(nentry);
  }

  async.eachSeries(ndata,
    function(entry, onCompleted){
      entry.simple_tag = typeof req.headers[config.tag] !== 'undefined' ? req.headers[config.tag] : 'none';
      entry.simple_timestamp = date.getTime();
      //console.log(entry);
      db.logging.save(entry, function(err, result){
        if (typeof err !== 'undefined' && err || result === null) {
          console.log('error');
          console.log(err);
        }
      });
    },
    function(err){
      res.json({status: 'completed'});
      res.end();
    });
})

app.options('/query', function(req, res, next) { res.sendStatus(200); });
app.post('/query', jsonParser, function(req, res) {
  let target = typeof req.body.targets !== 'undefined' ? req.body.targets[0].target : '';
  let stag = '';
  if (typeof target !== 'undefined' && target.includes(':::')) {
    let parts = target.split(':::');
    stag = parts[0];
    target = parts[1];
  }
  let dFrom = moment(req.body.range.from).toDate().getTime();
  let dTo = moment(req.body.range.to).toDate().getTime();

  db.logging.find({$and: [{simple_timestamp: {$gte: dFrom}}, {simple_timestamp: {$lte: dTo}}]}, function(err, items) {
    if (typeof err !== 'undefined' && err || items === null) {
      res.json([]);
    }
    else {
      let ret = [];
      let points = [];
      for(let i = 0; i < items.length; i += 1) {
        let item = items[i];
        for (let key in item) {
          let valid = false;
          if (key === target) {
            valid = true;
          }
          else {
            continue;
          }
          if (stag.length > 0) {
            if (item['simple_tag'] === stag) {
              valid = true;
            }
            else {
              continue;
            }
          }
          if (valid) {
            let value = item[key];
            let pair = [];
            pair.push(value);
            pair.push(moment(item['simple_timestamp']).toDate().getTime());
            points.push(pair);
          }
        }
      }
      ret.push({target: req.body.targets[0].target, datapoints: points});
      res.json(ret);
    }
  });
});

app.options('/search', function(req, res, next) { res.sendStatus(200); });
app.post('/search', jsonParser, function(req, res) {
  db.logging.find({}).sort({simple_timestamp: 1}).limit(10000, function(err, items){
    if (typeof err !== 'undefined' && err || items === null)
    {
      res.json([]);
    }
    else
    {
      let ret = [];
      for (let i = 0; i < items.length; i += 1) {
        let item = items[i];
        for (let key in item) {
          if (key !== '_id' && key !== 'simple_tag' && key !== 'simple_timestamp') {
            let found = false;
            for(let j = 0; j < ret.length; j += 1) {
              if (ret[j] === key) {
                found = true;
                break;
              }
            }
            if (!found) {
              ret.push(key);
            }
          }
        }
      }
      for (let i = 0; i < items.length; i += 1) {
        let item = items[i];
        for (let key in item) {
          if (key !== '_id' && key !== 'simple_tag' && key !== 'simple_timestamp') {
            let nkey = item['simple_tag'] + ':::' + key;
            let found = false;
            for(let j = 0; j < ret.length; j += 1) {
              if (ret[j] === nkey) {
                found = true;
                break;
              }
            }
            if (!found) {
              ret.push(nkey);
            }
          }
        }
      }
      res.json(ret);
    }
  });
});

app.options('/annotations', function(req, res, next) { res.sendStatus(200); });
app.post('/annotations', function(req, res) {
  res.json([]);
});

app.options('/tag-keys', function(req, res, next) { res.sendStatus(200); });
app.post('/tag-keys', function(req, res) {
  res.json([]);
});

app.options('/tag-values', function(req, res, next) { res.sendStatus(200); });
app.post('/tag-values', function(req, res) {
  res.json([]);
});

cron.schedule('* * * * *', () => {
  let date = new Date();
  let tm = date.getTime() - config.retention_time;
  db.logging.remove({simple_timestamp : {$lte: tm}}, function(err, result){
    //console.log('wipe out the old records');
  });
});

app.listen(port, () => console.log(`Log Aggregator listening at http://localhost:${port}`))
